import Head from 'next/head'
import Layout, {siteTitle} from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from 'next/link'
import Date from '../components/date'
import {getSortedPostsData} from '../lib/posts'
import Image from 'next/image'


export default function List({allPostsData}) {
  return (
    <Layout list>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={`d-flex justify-content-center ${utilStyles.headingLg}`}>Blog</h2>
        <ul>
          {allPostsData.map(({id, date, title, image}) => (
            <li key={id}>
              <div className="card mb-3" style={{maxWidth: "540px;"}}>
                <div className="row g-0">
                  <div className="col-md-4">
                    <div style={{width: '100%', height: '100%', position: 'relative'}}>
                      <Image
                        priority
                        src={`/images/${image}`}
                        className='card-img-top'
                        alt={image}
                        layout={'fill'}
                      />
                    </div>
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">{title}</h5>
                      <p className="card-text">
                        <small className="text-muted">
                          <Date dateString={date}/>
                        </small>
                      </p>
                      <Link href={`/posts/${id}`}>
                        <a className="btn btn-primary">View</a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}
