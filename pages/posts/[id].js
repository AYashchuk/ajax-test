import Layout from '../../components/layout'
import {getAllPostIds, getPostData} from "../../lib/posts";
import Head from 'next/head'
import Date from '../../components/date'
import Image from "next/image";
import utilStyles from "../../styles/utils.module.css";
import {useEffect, useState} from "react";

export default function Post({postData}) {
  const [userData, setUserData] = useState({});

  useEffect(() => {
    fetch('http://api.db-ip.com/v2/free/self')
      .then(resp => resp.json())
      .then(setUserData)
      .catch(() => setUserData({error: true}))
  }, [])

  return (
    <Layout>
      <Head>
        <meta name="keywords" content={`${postData.keyword}`}/>
        <meta name="description" content={"in this page we discuss different types of rendering "}/>
        <title>{postData.title}</title>
      </Head>
      <div className="container">
        <div className="row justify-content-md-center">
          <Image
            priority
            src={`/images/${postData.image}`}
            className={utilStyles.borderCircle}
            height={200}
            width={200}
            alt={postData.image}
          />
        </div>
        <br/>
        <b>
          {postData.title}
        </b>
        {!userData.error && (
          <div className="row">
            <div className="col-md-auto">
              User data:
            </div>
            <div className="col-md-auto">
              <code>
                <pre>
                  {JSON.stringify(userData, null, ' ')}
                </pre>
              </code>
            </div>
          </div>)}
        <br/>
        {postData.id}
        <br/>
        <Date dateString={postData.date}/>
        <br/>
        <div dangerouslySetInnerHTML={{__html: postData.contentHtml}}/>
      </div>
    </Layout>
  )
}

export async function getStaticPaths() {
  const paths = getAllPostIds()
  return {
    paths,
    fallback: false
  }
}

export async function getStaticProps({params}) {
  const postData = await getPostData(params.id)
  return {
    props: {
      postData
    }
  }
}
