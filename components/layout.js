import Head from 'next/head'
import styles from './layout.module.css'
import Link from 'next/link'

export const siteTitle = "Blog"

export default function Layout({ children, list }) {
  return (
    <div className={styles.container}>
      <Head>
        <script src="/bootstrap.js"></script>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
      </Head>
      <main>{children}</main>
      {!list && (
        <div className={styles.backToHome}>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </div>
      )}
    </div>
  )
}
