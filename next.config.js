module.exports = {
  webpack: (config, options) => {
    config.devServer = {
      proxy: {
        '/api': 'https://ajax.systems/blog/',
      },
    }


    return config
  },
}
